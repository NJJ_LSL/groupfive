package com.compass.mobilesafe.test;

import java.util.List;

import android.test.AndroidTestCase;

import com.compass.mobilesafe.domain.AppInfo;
import com.compass.mobilesafe.engine.AppInfoProvider;

public class TestAppInfoProvider extends AndroidTestCase {
	
	public void testGetAllApp() throws Exception{
		List<AppInfo> infos = AppInfoProvider.getAppInfos(getContext());
		for(AppInfo info:infos){
			System.out.println(info.toString());
		}
		
	}
}
