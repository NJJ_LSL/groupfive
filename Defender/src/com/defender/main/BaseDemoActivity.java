package com.defender.main;

import com.defender.base.BaseActivity;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class BaseDemoActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
		initView();
		setTitle("这是一个继承了base的activity");
	}

	@Override
	public void btnDetialOnClick() {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "detail", Toast.LENGTH_LONG).show();
	}

	@Override
	public void btnBackOnClick() {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "back", Toast.LENGTH_LONG).show();
	}

	@Override
	public View setContentView() {
		// TODO Auto-generated method stub
		return View.inflate(this, R.layout.activity_base_demo, null);
	}
}
