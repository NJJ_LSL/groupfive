package com.defender.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/*
author：Compass
*/
public abstract class DBOpenhelper extends SQLiteOpenHelper {
	/**用于创建数据库*/
	public DBOpenhelper(Context context) {
		super(context, "defender.db", null, 1);
		// TODO Auto-generated constructor stub
	}
	
	/**当数据库已经成功创建，将回调此方法 适用于创建表*/
	@Override
	public abstract void onCreate(SQLiteDatabase db);
	
	/**数据库版本升级*/
	@Override
	public abstract void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);

}
