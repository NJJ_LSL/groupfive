package com.defender.base;

import com.defender.main.R;
import com.defender.main.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public abstract class BaseActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//设置没有标题
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_base);
		initView();
	}
	
	/**
	 * @method: 初始化view*/
	public void initView(){
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_detial = (Button) findViewById(R.id.btn_detail);
		tv_title = (TextView) findViewById(R.id.tv_title);
		
		//添加子页面的布局文件
		ll_child_context = (LinearLayout) findViewById(R.id.ll_child_context);
		View childView = setContentView(); 
		//导包注意事项，需要导入线性布局的包
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ll_child_context.addView(childView, params);
	}
	
	
	/**
	 * method: 监听back、detial按钮以及标题文字*/
	private void titleListener(){
		btn_back.setOnClickListener(myListener);
		btn_detial.setOnClickListener(myListener);
	}
	
	OnClickListener myListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				btnBackOnClick();
				break;
			case R.id.btn_detail:
				btnDetialOnClick();
				break;
			default:
				break;
			}
		}
	};
	/**method:设置返回按钮是否可见
	 * 用法:传入View.GONE为当前控件不可见*/
	public void setBackVisible(int visible) {
		btn_back.setVisibility(visible);
	}
	/**method:设置详细按钮是否可见
	 * 用法:传入View.GONE为当前控件不可见*/
	public void setDetialVisible(int visible) {
		btn_back.setVisibility(visible);
	}
	/**
	 * method:设置当前功能的标题*/
	public void setTitle(String title){
		tv_title.setText(title);
	}
	private Button btn_back;
	private Button btn_detial;
	private TextView tv_title;
	private LinearLayout ll_child_context;
	/**method:详细按钮的具体实现*/
	public abstract void btnDetialOnClick();
	/**method:返回按钮的具体实现*/
	public abstract void btnBackOnClick();
	/**
	 * 由子类实现，加载布局文件
	 * @return View*/
	public abstract View setContentView();
	
}
